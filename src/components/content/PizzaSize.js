import { Typography, Card, CardActionArea, CardContent, CardActions, Button , Grid} from "@mui/material";
import { useEffect, useState } from 'react'

const PizzaSize = () =>{
    const [statusBtnS, setStatusBtnS] = useState("warning");
    const [statusBtnM, setStatusBtnM] = useState("warning");
    const [statusBtnL, setStatusBtnL] = useState("warning");

    const [pizzaSize, setPizzaSize] = useState("");

    const onBtnPizzaS = () =>{
        setStatusBtnS("success");
        setStatusBtnM("warning");
        setStatusBtnL("warning");
        setPizzaSize("S");
        console.log(pizzaSize);
    }

    const onBtnPizzaM = () =>{
        setStatusBtnS("warning");
        setStatusBtnM("success");
        setStatusBtnL("warning");
        setPizzaSize("M");
        console.log(pizzaSize);
    }

    const onBtnPizzaL = () =>{
        setStatusBtnS("warning");
        setStatusBtnM("warning");
        setStatusBtnL("success");
        setPizzaSize("L");
        console.log(pizzaSize);
    }
    return(
        <>
            <Typography variant="h5" align="center" color='warning.main' marginTop={5}>
                <b>Chọn kích cỡ pizza   </b>
            </Typography>
            <p align='center' color="warning.main">chọn kích cỡ pizza mà bạn muốn.</p>
            <Grid container justifyContent={"space-between"}>
                <Grid style={{width: "33%"}}>
                <Card>
                    <CardActionArea>
                        <Typography align="center" padding={3} gutterBottom variant="h5" component="div" style={{backgroundColor: "#ffc107"}} >
                            <b>S(Small)</b>
                        </Typography>
                        <CardContent>
                        <Typography padding={1} variant="body1" align="center" style={{borderBottom: "1px solid #9999"}}>
                            Đường kính <b>20Cm</b>
                        </Typography>
                        <Typography padding={1}  variant="body1" align="center" style={{borderBottom: "1px solid #9999"}}>
                            Sườn nướng <b>2</b>
                        </Typography>
                        <Typography padding={1}  variant="body1" align="center" style={{borderBottom: "1px solid #9999"}}>
                            Salad <b>200gram</b>
                        </Typography>
                        <Typography  padding={1} variant="body1" align="center" style={{borderBottom: "1px solid #9999"}}>
                            Nước ngọt <b>2</b>
                        </Typography>
                        <Typography padding={1}  variant="h5" align="center">
                            <b>150.000</b>
                        </Typography>
                        <Typography padding={1}  variant="body1" align="center" style={{borderBottom: "1px solid #9999"}}>
                            (VND)
                        </Typography>
                        </CardContent>
                    </CardActionArea>
                    <CardActions>
                        <Button onClick={onBtnPizzaS} color={statusBtnS} fullWidth size="small" variant="contained">
                        CHỌN
                        </Button>
                    </CardActions>
                </Card>
                </Grid>
                <Grid style={{width: "33%"}}>
                <Card>
                    <CardActionArea>
                        <Typography align="center" padding={3} gutterBottom variant="h5" component="div" style={{backgroundColor: "#ffc107"}}>
                            <b>M(Medium)</b>
                        </Typography>
                        <CardContent>
                        <Typography padding={1} variant="body1" align="center" style={{borderBottom: "1px solid #9999"}}>
                            Đường kính <b>25Cm</b>
                        </Typography>
                        <Typography padding={1}  variant="body1" align="center" style={{borderBottom: "1px solid #9999"}}>
                            Sườn nướng <b>4</b>
                        </Typography>
                        <Typography padding={1}  variant="body1" align="center" style={{borderBottom: "1px solid #9999"}}>
                            Salad <b>300gram</b>
                        </Typography>
                        <Typography  padding={1} variant="body1" align="center" style={{borderBottom: "1px solid #9999"}}>
                            Nước ngọt <b>3</b>
                        </Typography>
                        <Typography padding={1}  variant="h5" align="center">
                            <b>200.000</b>
                        </Typography>
                        <Typography padding={1}  variant="body1" align="center" style={{borderBottom: "1px solid #9999"}}>
                            (VND)
                        </Typography>
                        </CardContent>
                    </CardActionArea>
                    <CardActions>
                        <Button onClick={onBtnPizzaM} color={statusBtnM} fullWidth size="small" variant="contained">
                        CHỌN
                        </Button>
                    </CardActions>
                </Card>
                </Grid>
                <Grid style={{width: "33%"}}>
                <Card>
                    <CardActionArea>
                        <Typography align="center" padding={3} gutterBottom variant="h5" component="div" style={{backgroundColor: "#ffc107"}}>
                            <b>L(Large)</b>
                        </Typography>
                        <CardContent>
                        <Typography padding={1} variant="body1" align="center" style={{borderBottom: "1px solid #9999"}}>
                            Đường kính <b>30Cm</b>
                        </Typography>
                        <Typography padding={1}  variant="body1" align="center" style={{borderBottom: "1px solid #9999"}}>
                            Sườn nướng <b>8</b>
                        </Typography>
                        <Typography padding={1}  variant="body1" align="center" style={{borderBottom: "1px solid #9999"}}>
                            Salad <b>500gram</b>
                        </Typography>
                        <Typography  padding={1} variant="body1" align="center" style={{borderBottom: "1px solid #9999"}}>
                            Nước ngọt <b>4</b>
                        </Typography>
                        <Typography padding={1}  variant="h5" align="center">
                            <b>250.000</b>
                        </Typography>
                        <Typography padding={1}  variant="body1" align="center" style={{borderBottom: "1px solid #9999"}}>
                            (VND)
                        </Typography>
                        </CardContent>
                    </CardActionArea>
                    <CardActions>
                        <Button onClick={onBtnPizzaL} color={statusBtnL} fullWidth size="small" variant="contained">
                        CHỌN
                        </Button>
                    </CardActions>
                </Card>
                </Grid>
            </Grid>
        </>
    )
}

export default PizzaSize;