import {Grid, Typography}  from '@mui/material'

const Info = () =>{
    return(
        <>
            <Typography variant='h5' align='center' marginTop={5} color="warning.main">
                <b>Tại sao bạn nên chọn mua tại Pizza 365 !!</b>
            </Typography>

            <Grid container marginTop={3}>
                <Grid item xs={3} style={{backgroundColor:"#e18e40"}} padding={3}>
                    <h3>Đa dạng</h3>
                    <p>Số lượng pizza đa dạng, có đầy đủ các loại pizza đang hot nhất hiện nay.</p>
                </Grid>

                <Grid item xs={3} style={{backgroundColor:"#df842e"}} padding={3}>
                    <h3>Chất lượng</h3>
                    <p>Nguyên liệu sạch 100%, nguồn gốc xuất sứ rõ ràng, quy trình chế biến đảm bảo vệ sinh an toàn thực phẩm.</p>
                </Grid>
         
                <Grid item xs={3} style={{backgroundColor:"#e17a18"}}  padding={3}>
                    <h3>Hương vị</h3>
                    <p>Đảm bảo hương vị ngon, độc đáo, lạ mà bạn chỉ có thể thưởng thức được tại Pizza 365.</p>
                </Grid>
                       
                <Grid item xs={3} style={{backgroundColor:"#dd710a"}}  padding={3}>
                    <h3>Dịch vụ</h3>
                    <p>Nhân viên thân thiện, nhà hàng hiện đại. Dịch vụ giao hàng nhanh chóng, chất lượng tân tiến.</p>
                </Grid>

            </Grid>

           
           
        </>
    )
}

export default Info;