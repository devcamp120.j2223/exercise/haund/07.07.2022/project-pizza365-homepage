import { Container } from "reactstrap";
import CarouselSlide from "./components/CarouselSlide";
import Content from "./components/Content";
import Footer from "./components/Footer";
import Header from "./components/Header";


function App() {
  return (
    <div style={{backgroundColor: "#e5c7ba"}}>
    <Header/>
    
    <Container>
        <CarouselSlide/>
        <Content/>
    </Container>

    <Footer/>
    
    </div>
  );
}

export default App;
